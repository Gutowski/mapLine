angular.module('lineApp').service('TripService', ['$http', 'tripUrl', function($http, tripUrl) {
	
	var trips = {};
	
	var deleteFromArray = function(trip) {
		var index = trips.map(function(trip) { return trip['id']; }).indexOf(trip.id);
		console.log(index);
		if(index > -1) {
			trips.splice(index, 1);
		}
	};
	
	var updateOrPushToArray = function(trip) {
		var index = trips.map(function(trip) { return trip['id']; }).indexOf(trip.id);
		if(index > -1) {
			trips[index].name = trip.name;
			trips[index].description = trip.description;
		}
		else {
			trips.push(trip);
		}
	};
	
	return {	
		getAll: function() {
			return  $http.get(tripUrl).then(function successCallback(response) {
						trips = response.data._embedded.trips;
						return trips;
					}, function errorCallback(error) {
						trips.error = error;
						return trips;
					});
		},
		
		save: function(tripData) {
			$http.post(tripUrl, tripData).then(function successCallback(response) {
				trips.push(response.data);
				console.log(trips);
			}, function errorCallback(error) {
				
			});
		},
		
		getById: function(idNo) {
			var trip = _.findWhere(trips, {id: idNo});
			if(typeof trip === 'undefined') {
				var tripUrlWithId = tripUrl + idNo;
				console.log(tripUrlWithId);
				return $http.get(tripUrlWithId).then(function successCallback(response) {
					return response.data;
				}, function errorCallback(response) {
					return {};
				});
			}
			else {
				return trip;
			}
		},
		
		deleteById: function(trip) {
			var tripUrlWithId = tripUrl + trip.id;
			$http.delete(tripUrlWithId, trip).then(function successCallback(response) {
				deleteFromArray(trip);
			}, function errorCallback(response) {

			});
		},
		
		updateTrip: function(trip) {
			var tripUrlWithId = tripUrl + trip.id;
			$http.put(tripUrlWithId, trip).then(function successCallback(resposne) {
				updateOrPushToArray(trip);
			}, function errorCallback(resposne) {
				
			});
		}
	};
	
}]);
