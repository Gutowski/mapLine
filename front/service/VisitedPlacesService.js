angular.module('lineApp').service('VisitedPlacesService', ['$http', 'visitedPlacesUrl', function($http, visitedPlacesUrl) {
	var visitedPlaces = {};
	
	var addIdToLink = function(tripId) {
		var visitedPlacesUrlWithId = visitedPlacesUrl;
		visitedPlacesUrlWithId = visitedPlacesUrlWithId.replace(":id", tripId);
		return visitedPlacesUrlWithId;
	};
	
	return {
		
		getAllFromTripId: function(tripId) {
			var visitedPlacesUrlWithId = addIdToLink(tripId);
			return $http.get(visitedPlacesUrlWithId).then(function successCallback(response) {
				visitedPlaces = response.data._embedded.visitedPlaces;
				return visitedPlaces;
			}, function errorCallback(error) {
				
			});
		}
		
	};
}]);