angular.module('lineApp', ['ui.bootstrap', 'ui.router']);

angular.module('lineApp').constant("tripUrl", "http://localhost:8090/trips/");
angular.module('lineApp').constant("visitedPlacesUrl", "http://localhost:8090/trips/:id/visitedPlaces");
