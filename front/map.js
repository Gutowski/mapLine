var lineApp = angular.module('lineApp');

lineApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	
    // uiGmapGoogleMapApiProvider.configure({
        // key: 'AIzaSyD6N_9FlrvGzR2NLxkr93-Nje_GRKm5PpM',
        // v: '3.20', //defaults to latest 3.X anyhow
        // libraries: 'weather,geometry,visualization'
    // });	
	
	$urlRouterProvider.otherwise("/home");
	$stateProvider
		.state('home', {
			url: '/home',
			templateUrl: 'partial/homePartial.html'
		})
		.state('map', {
			url: '/map',
			templateUrl: 'partial/mapPartial.html',
			controller: 'mapCtrl',
			resolve: {
				Trips: function($stateParams, TripService) {
					return TripService.getAll();
				}
			}
		})
		.state('visitedPlaces', {
			url: '/visitedPlace/:tripId',
			templateUrl: 'partial/visitedPlacePartial.html',
			controller: 'visitedPlacesCtrl',
			resolve: {
				VisitedPlaces: function($stateParams, VisitedPlacesService) {
					return VisitedPlacesService.getAllFromTripId($stateParams.tripId);
				},
				TripInfo: function($stateParams, TripService) {
					return TripService.getById($stateParams.tripId);
				}
			}
		})
		.state('about', {
			url: '/about',
			templateUrl: 'partial/aboutPartial.html'
		})
		.state('about.author', {
			url:'/about/author',
			templateUrl: 'partial/aboutAuthorPartial.html'
		})
		.state('about.project', {
			url: '/about/project',
			templateUrl: 'partial/aboutProjectPartial.html'	
		});
		
}]);
lineApp.controller("lineCtrl", function () {
	
});