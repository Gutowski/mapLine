angular.module('lineApp').controller("visitedPlacesCtrl", ['$scope', '$uibModal', 'VisitedPlaces', 'TripInfo', 'VisitedPlacesService', function($scope, $uibModal, VisitedPlaces, TripInfo, VisitedPlacesService) {
	$scope.visitedPlaces = VisitedPlaces;
	$scope.tripInfo = TripInfo;
}]);