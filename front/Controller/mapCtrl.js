angular.module('lineApp').controller("mapCtrl", ['$scope', '$state', '$uibModal', 'Trips', 'TripService', function($scope, $state, $uibModal, Trips, TripService) {
	
	$scope.trips = Trips;
	console.log($scope.trips);
	var modalInstance = {};
	$scope.modal = {};
	$scope.modal.trip = {};
	$scope.choosenTripObj = {};
	$scope.choosenIndex = -1;
	
	$scope.go = function(where) {
		console.log(where);
		$state.go(where, {tripId: $scope.choosenTripObj.id });
	};
	$scope.addTrip = function(tripData) {
		if(angular.isUndefined(tripData.id)) {
			console.log(tripData);
			TripService.save(tripData);
		}
		else {
			TripService.updateTrip(tripData);
		}
		$scope.modalClose();
	};
	
	$scope.deleteTrip = function(trip) {
		TripService.deleteById(trip);
		$scope.choosenIndex = -1;
		$scope.choosenTripObj = {};
	};
	
	$scope.modalOpen = function(trip) {
		$scope.modal.trip = angular.copy(trip);
		modalInstance = $uibModal.open({
			templateUrl: 'modal/modalTrip.html',
			size: 'lg',
			scope: $scope
		});
	};
	
	$scope.modalClose = function() {
		$scope.modal.trip = {};
		modalInstance.close();
	};
	
	$scope.chooseTrip = function(trip, index) {
		console.log(index);
		console.log(trip);
		$scope.choosenTripObj = trip;
		$scope.choosenIndex = index;
	};
	
}]);
