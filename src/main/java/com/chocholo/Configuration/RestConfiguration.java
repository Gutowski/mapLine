package com.chocholo.Configuration;

import com.chocholo.Entity.Trip;
import com.chocholo.Entity.VisitedPlace;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

/**
 * Created by gutek on 02.12.16.
 */
@Configuration
public class RestConfiguration extends RepositoryRestMvcConfiguration {
    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Trip.class, VisitedPlace.class);
    }
}
