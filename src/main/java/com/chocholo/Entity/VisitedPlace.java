package com.chocholo.Entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import sun.util.resources.CalendarData;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Created by chocholo on 16.11.16.
 */
@Entity
@Data
public class VisitedPlace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @NotNull
    int numberInQueue;

    LocalDate time;

    @NotNull
    GeoPosition geoPosition;

    @NotNull
    @Length(max = 70)
    String placeName;

    String shortDescription;

    String longDescription;

    @ManyToOne
    @JoinColumn(name = "trip_id")
    Trip trip;
}
