package com.chocholo.Entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 * Created by chocholo on 16.11.16.
 */
@Entity
@Data
public class GoogleLocalGeo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @NotNull
    String name;
    @NotNull
    GeoPosition geoPosition;
}
