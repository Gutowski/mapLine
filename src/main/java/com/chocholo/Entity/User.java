package com.chocholo.Entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by chocholo on 16.11.16.
 */
//@Entity
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;
    @NotNull
    String login;
    @NotNull
    String password;
    @NotNull
    String email;
    @NotNull
    String name;
    @NotNull
    String surname;
    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    LocalDate dateOfBirth;
    @OneToMany(mappedBy = "user")
    List<Trip> trips;
    @NotNull
    LocalDateTime registrationDate;
}
