package com.chocholo.Entity;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Created by chocholo on 16.11.16.
 */
@Embeddable
@Data
public class GeoPosition {
    @NotNull
    long latitude;
    @NotNull
    long longitude;
}
