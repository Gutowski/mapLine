package com.chocholo.Entity;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by chocholo on 16.11.16.
 */
@Entity
@Data
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @Column
    @NotNull(message = "The name of your table cannot be null")
    @Length(max = 70, message = "The trip's name cannot be longer than 70")
    String name;
    String description;

    @OneToMany(mappedBy = "trip")
    List<VisitedPlace> visitedPlaces;
};
