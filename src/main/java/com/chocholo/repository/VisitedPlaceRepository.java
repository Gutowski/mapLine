package com.chocholo.repository;

import com.chocholo.Entity.VisitedPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by chocholo on 16.11.16.
 */
@Repository
public interface VisitedPlaceRepository extends JpaRepository<VisitedPlace, Long> {

}
