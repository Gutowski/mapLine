package com.chocholo.repository;

import com.chocholo.Entity.Trip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

/**
 * Created by chocholo on 16.11.16.
 */
@Repository
@CrossOrigin
public interface TripRepository extends JpaRepository<Trip, Long> {
}
