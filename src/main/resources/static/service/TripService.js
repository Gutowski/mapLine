angular.module('lineApp').service('TripService', ['$http', 'tripUrl', function($http, tripUrl) {
	
	var trips = {};
	
	var getTrips = function() {
		$http.get(tripUrl).then(function successCallback(response) {
			trips = response.data._embedded.trips;
		}, function errorCallback(error) {
			trips.error = error;
		});
	};
		
	getTrips();
	
	return {	
		getAll: function() {
			return trips;
		},
		save: function(tripData) {
		    console(tripData);
			$http.post(tripUrl, tripData).then(function successCallback(response) {
				trips.push(response.data);
			}, function errorCallback(error) {
				
			});
		},
		getById: function(idNo) {
			return _.findWhere(trips, {id: idNo});
		},
		deleteById: function(id) {
			var index = trips.map(function(trip) { return trip['id']; }).indexOf(id);
			console.log(index);
			if(index > -1) {
				trips.splice(index, 1);
				return true;
			}
			return false;
		},
		updateTrip: function(trip) {
			var index = trips.map(function(trip) { return trip['id']; }).indexOf(trip.id);
			if(index > -1) {
				trips[index].name = trip.name;
			}
			else {
				trips.push(trip);
			}
		}
	};
	
}]);
