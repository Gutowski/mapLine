angular.module('lineApp').controller("mapCtrl", ['$scope', '$uibModal', 'Trips', 'TripService', function($scope, $uibModal, Trips, TripService) {
	
	$scope.trips = Trips;
	var modalInstance = {};
	$scope.modal = {};
	$scope.modal.trip = {};
	$scope.choosenTripObj = {};
	$scope.choosenIndex = -1;
	
	$scope.addTrip = function(tripData) {
		if(angular.isUndefined(tripData.id)) {
			TripService.save(tripData);
		}
		else {
			TripService.updateTrip(tripData);
		}
		$scope.modalClose();
	};
	
	$scope.deleteTrip = function(trip) {
		if(TripService.deleteById(trip.id)) {
			$scope.choosenIndex = -1;
			$scope.choosenTripObj = {};
		}
	};
	
	$scope.modalOpen = function(trip) {
		$scope.modal.trip = angular.copy(trip);
		modalInstance = $uibModal.open({
			templateUrl: 'modal/modalTrip.html',
			size: 'lg',
			scope: $scope
		});
	};
	
	$scope.modalClose = function() {
		$scope.modal.trip = {};
		modalInstance.close();
	};
	
	$scope.chooseTrip = function(trip, index) {
		console.log(index);
		console.log(trip);
		$scope.choosenTripObj = trip;
		$scope.choosenIndex = index;
	};
	
}]);
